import styled from "styled-components"

/* HEADER COMPONENTS */


export const HeaderMainContainer = styled.div`
  border-right: solid 1px black;
  display:flex;
  width: 250px;
  height: 100vh;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  background-image: linear-gradient(to bottom right, rgba(0, 0, 0, 0.2), #AAD);

  @media(max-width:600px)
  {
  width: 100vw;
  height: 10vh;
  border-bottom: solid 1px black;
  border-right: solid 0px black;
  flex-direction: row;
  border-bottom: 1px solid black;
  margin:0px;
  }

`

export const HeaderPictureContainer = styled.img`
  border: 6px solid white;
  border-radius:50%;
  height: 180px;
  width: 180px;
  src: "IMG_2882.jpg";

  @media(max-width:600px)
  {
  width: 40px;
  height: 40px;
  border: 3px solid white;
  }
`

export const HeaderInnerContainer = styled.div`
  height: 320px;
  width: 220px;

  @media(max-width:600px)
  {
    display: flex;
    flex-direction: row;
    height: 80px;
    width: 280px;
  }
  
`

export const HeaderMenus = styled.div`
font-size: 20px;
color: #222;
border-bottom: solid 0.5px #777;
height: 30px;
margin: 15px;
text-align: left;
padding:5px;

:hover{
  cursor: pointer;
  background-color: #CFD;
  transition: 1.0s;
  color: #111;
}

@media(max-width:600px)
  {
    width: 20%;
    font-size: 15px; 
    text-align: center;
    margin: 15px 5px;
    padding:3px;
  }

`
