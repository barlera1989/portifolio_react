import React from "react"
import { useHistory } from 'react-router-dom'

import { AiOutlineHome } from 'react-icons/ai';
import { FaReact, FaNewspaper } from 'react-icons/fa'
import { BsFillChatSquareDotsFill } from 'react-icons/bs'

import {
  HeaderMainContainer,
  HeaderInnerContainer,
  HeaderMenus,
  HeaderPictureContainer,

} from "./styles"

const Header = () => {
  const history = useHistory()


  return (
    <HeaderMainContainer>
      <HeaderPictureContainer src='rafa.jpg'>

      </HeaderPictureContainer>
      <HeaderInnerContainer>

        <HeaderMenus onClick={() => history.push('/')}>
          <AiOutlineHome></AiOutlineHome>
          {'\u00A0'}Home
          </HeaderMenus>

        <HeaderMenus onClick={() => history.push('/about')}>
          <FaReact></FaReact>
          {'\u00A0'}About
          </HeaderMenus>

        <HeaderMenus onClick={() => history.push('/projects')}>
          <FaNewspaper></FaNewspaper>
          {'\u00A0'}Projects
          </HeaderMenus>

        <HeaderMenus onClick={() => history.push('/contact')}>
          <BsFillChatSquareDotsFill></BsFillChatSquareDotsFill>
          {'\u00A0'}Contact
          </HeaderMenus>
      </HeaderInnerContainer>
    </HeaderMainContainer>
  )
}


export default Header