import React from 'react'
import { GoogleMap, useJsApiLoader } from '@react-google-maps/api';

const containerStyle = {
  width: '250px',
  height: '200px'
};

const center = {
  lat: -23.608886907055638,
  lng: -46.62672155920158
};

function MapContainer() {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: 'AIzaSyCNaobl0H3wEYwArQJn-5vLRwr5GMLM5b8'
  })

  /*   const [map, setMap] = React.useState(null) */

  /*  const onLoad = React.useCallback(function callback(map) {
     const bounds = new window.google.maps.LatLngBounds();
     map.fitBounds(bounds);
     setMap(map)
   }, [])
 
   const onUnmount = React.useCallback(function callback(map) {
     setMap(null)
   }, []) */

  return isLoaded ? (
    <GoogleMap
      mapContainerStyle={containerStyle}
      center={center}
      zoom={15}
    /* onLoad={onLoad}
    onUnmount={onUnmount} */
    >
      { /* Child components, such as markers, info windows, etc. */}
      <></>
    </GoogleMap>
  ) : <></>
}

export default React.memo(MapContainer)
