import styled from "styled-components"
import { Form, Button, Input } from 'antd';


export const ContactPagePositioning = styled.div`
display:flex;
align-items: center;
justify-content: center;
width:70vw;
@media(max-width:600px)
  {
    width:100vw;
  }
`

export const ContactPageContainer = styled.div`
height: 90vh;
display: flex;
flex-direction: row;
align-items:center;
@media(max-width:600px)
  {
    flex-direction: column;
    height:auto;
  }
`


export const ContactLeftContent = styled.div`
height:500px;
width:300px;
border: 2px solid lightgray;
border-radius:10px;
margin:10px;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
@media(max-width:600px)
  {
    margin:10px;
  }
`



export const ContactMapsContainer = styled.div`
width:250px;
height:200px;
border: 1px solid red;
margin:20px;
`


export const ContactContactsContainer = styled.div`
width:250px;
height:200px;
margin:10px;
display: flex;
flex-direction:column;
`

export const ContactContactsItems = styled.div`
display: flex;
flex-direction:row;
padding:5px;
`

export const ContactContactsImage = styled.div`
width:30px;
height:30px;
font-size:30px;
color: rgb(50,50,200);
`

export const ContactContactsMainText = styled.div`
font-size:25px;
color:black;
padding-bottom: 10px;
text-align: center;
`


export const ContactContactsText = styled.a`
width:200px;
height:30px;
font-size:${(props) => `${props.font_size}px`};;
color:black;
padding-left: 10px;
padding-top:3px;
`









export const ContactRightContent = styled.div`
height:500px;
width:300px;
border: 2px solid lightgray;
border-radius:10px;
margin:10px;
display: flex;
flex-direction: row;
justify-content:center;
align-items: center;
@media(max-width:600px)
  {
    margin:10px;
  }
`

export const ContactMailContainer = styled.div`
display: flex;
flex-direction: column;
`

export const ContactMailText = styled.div`
width:120px;
height:80px;
margin:10px;
font-size: 20px;
font-family:bangers;
:hover{
  cursor: pointer;
}
`

export const ContactMailImage = styled.img`
width:120px;
height:120px;
:hover{
  cursor: pointer;
  background-color: #CFD;
  transition: 1.0s;
  color: #111;
}
`

export const ContactFormsItem = styled(Form.Item)`
font-size: 20px;
font-family:bangers;
margin:10px;
margin-top:10px;
`

export const ContactFormsButton = styled(Button)`
background-color: #4499EE;
color: #EEEEEE;
font-size: 20px;
font-family:bangers;
position: relative;
left: 100px;
`

export const ContactFormsInput = styled(Input)`
width: 200px;

`

export const ContactFormsLargeInput = styled(Input)`
width: 200px;
height: 100px;
`