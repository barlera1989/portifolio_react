import React, { useState } from "react"
import emailjs from 'emailjs-com';
import { notification } from 'antd';
import MapContainer from './maps'

import { FaFacebook, FaWhatsapp } from "react-icons/fa"
import { AiFillGithub, AiFillLinkedin } from "react-icons/ai"

/* 5.2 - CONTACTS LIST : WHATSAPP, FACEBOOK, LINKEDIN, GITHUB */
import {
  ContactPagePositioning,
  ContactLeftContent,
  ContactRightContent,
  ContactPageContainer,
  ContactMailContainer,
  ContactMailText,
  ContactMailImage,
  ContactMapsContainer,
  ContactContactsContainer,
  ContactContactsImage,
  ContactContactsText,
  ContactContactsMainText,
  ContactContactsItems,
  ContactFormsItem,
  ContactFormsInput,
  ContactFormsLargeInput
} from "./styles/contact"

const ContactPage = () => {
  const [showMail, setShowMail] = useState(true)
  const [showForms, setShowForms] = useState(false)

  const reloadPage = () => {
    document.location.reload()
  }


  const changeToForms = () => {
    setShowMail(false)
    setShowForms(true)
  }


  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_3oamuxe', 'template_y9gju1u', e.target, 'user_qa864DWEAST32nLHjkrsE')
      .then((result) => {
        notification.success({
          key: "character",
          message: 'OK!',
          description: 'Seu email foi enviado!',

        })
        setTimeout(reloadPage, 500)
      }, (error) => {
        return notification.error({
          key: "character",
          message: 'Erro!',
          description: 'Personagem já adicionado',
        });
      });

  }


  const contactObject = [
    {
      image: <FaWhatsapp></FaWhatsapp>,
      text: '55 11 964737616',
      font_size: '20'
    },

    {
      image: <FaFacebook></FaFacebook>,
      link: 'https://www.facebook.com/rafael.barleraalves/',
      text: 'LINK TO FACEBOOK',
      font_size: '15'
    },

    {
      image: <AiFillGithub></AiFillGithub>,
      link: 'https://github.com/Barlera1989',
      text: 'LINK TO GIT HUB',
      font_size: '15'
    },

    {
      image: <AiFillLinkedin></AiFillLinkedin>,
      link: 'https://www.linkedin.com/in/rafael-barlera/',
      text: 'LINK TO LINKEDIN',
      font_size: '15'
    },
  ]



  return (
    <ContactPagePositioning>
      <ContactPageContainer>


        <ContactLeftContent>
          <ContactMapsContainer>
            <MapContainer />
          </ContactMapsContainer>
          <ContactContactsContainer>
            <ContactContactsMainText>Contacts</ContactContactsMainText>

            {contactObject.map((contactObject, i) => (
              <ContactContactsItems key={i}>
                <ContactContactsImage>{contactObject.image}</ContactContactsImage>
                <ContactContactsText href={contactObject.link} font_size={contactObject.font_size}>{contactObject.text}</ContactContactsText>
              </ContactContactsItems>

            ))}
          </ContactContactsContainer>
        </ContactLeftContent>


        <ContactRightContent>
          <ContactMailContainer>


            {showMail && <ContactMailText onClick={() => changeToForms()}>SEND A EMAIL!<br />*CLICK IN THE EMAIL ICON*</ContactMailText>}
            {showMail && <ContactMailImage onClick={() => changeToForms()} src='gmail.png' />}

            {showForms && <form className="contact-form" onSubmit={sendEmail}>
              <ContactFormsItem>Name</ContactFormsItem>
              <ContactFormsInput type="text" name="name" />
              <ContactFormsItem>Email</ContactFormsItem>
              <ContactFormsInput type="email" name="email" />
              <ContactFormsItem>Message</ContactFormsItem>
              <ContactFormsLargeInput name="message" />
              <br />
              <input type="submit" value="Send" />
            </form>}
          </ContactMailContainer>
        </ContactRightContent>
      </ContactPageContainer>

    </ContactPagePositioning >
  )
}

export default ContactPage