import styled from "styled-components"

export const ProjectPagePositioning = styled.div`
display:flex;
align-items: center;
justify-content: center;
width:80vw;
@media(max-width:600px)
  {
    width:100vw;
  }
`

export const ProjectPageContainer = styled.div`
height: 90vh;
display: flex;
flex-direction: row;
align-items:center;
@media(max-width:600px)
  {
    flex-direction: column;
    height:auto;
  }
`


export const ProjectContent = styled.div`
height:500px;
width:300px;
border: 2px solid lightgray;
border-radius:10px;
margin:10px;
display: flex;
flex-direction: column;
justify-content:space-between;
@media(max-width:600px)
  {
    margin:10px;
  }
`

export const ProjectText = styled.div`
font-size: 15px;
text-align: left;
font-family:bangers;
`

export const ProjectTextTitle = styled.div`
font-size: 25px;
text-align: left;
font-family:bangers;
`

export const ProjectImageContainer = styled.div`
height: 160px;
display: flex;
flex-direction: row;
justify-content:space-evenly;
flex-wrap:wrap;
`

export const ProjectImages = styled.img`
height: 70px;
width: 100px;
border: 1px solid black;
:hover{
  cursor: pointer;
}
`