import React from "react"



import {
  ProjectPagePositioning,
  ProjectPageContainer,
  ProjectContent,
  ProjectText,
  ProjectTextTitle,
  ProjectImageContainer,
  ProjectImages
} from "./styles"

const ProjectPage = () => {

  return (
    <ProjectPagePositioning>
      <ProjectPageContainer>

        <ProjectContent>
          <ProjectTextTitle>
            PORTIFOLIO
            </ProjectTextTitle>
          <br />
          <ProjectText>
            This project was made with the purpose of showing
            some of my front-end skills that i learned so far.
            You can check it out in the cellphone too.
            This is single page application project was
            made with React using some libraries like redux,
            styled-components,react-router-dom, motion,
            google-maps-api, mailjs and antd. Deployed on vercel.
            <br />
              Repository:<span><a href={'https://gitlab.com/barlera1989/portifolio_react'}> click here!</a></span>
            <br />
              Page:<span><a href={'https://portifolio-react.vercel.app/'}> click here!</a></span>

          </ProjectText>
          <ProjectImageContainer>
            <ProjectImages src={'port1.png'} />
            <ProjectImages src={'port2.png'} />
          </ProjectImageContainer>
        </ProjectContent>

        <ProjectContent>
          <ProjectTextTitle>
            TECH ARCHIVE
            </ProjectTextTitle>
          <br />
          <ProjectText>
            An educational full api made with
            react and python / FlaskAlchemy.
            The main purpose was a page that give
            some of its profits to people that post news ,
            making this project self powered.
            In this project, i was a scrum master,
            where my function was make daily meetings,
            making the communication easier, making a scrum planning
            using trello and build some sketch using figma.
            <br />
              Front-end Repository:<span><a href={'https://github.com/Barlera1989/TechArchive-Front-End'}> click here!</a></span>
            <br />
              Back-end Repository:<span><a href={'https://github.com/Barlera1989/TechArchive-BackEnd'}> click here!</a></span>
            <br />
              Page:<span><a href={'https://tech-archive-3n090oes8.vercel.app/'}> click here!</a></span>
            <br />
              Figma sketch:<span><a href={'https://www.figma.com/file/Kx1jgZRJWXdNSWLHPlZmm7/Prototyping-in-Figma?node-id=0%3A1'}> click here!</a></span>

          </ProjectText>
          <ProjectImageContainer>
            <ProjectImages src={'tech1.png'} />
            <ProjectImages src={'tech2.png'} />
            <ProjectImages src={'tech3.png'} />

          </ProjectImageContainer>

        </ProjectContent>

        <ProjectContent>
          <ProjectTextTitle>
            BOOK BOOK
            </ProjectTextTitle>
          <br />
          <ProjectText>
            An educational front-end aplication using react.
            The main purpose learning about making connection between front-end to back-end,
            where the api was already made.
            We use insomnia to test connections and we use the library axios to make the connection on code.
            In this project i was a tech leader,
            where i made the initial configuration of the project,
            making some plans, organizing the folders and files in code, and sorting out tasks to do.

            <br />
              Repository:<span><a href={'https://gitlab.com/barlera1989/portifolio_react'}> click here!</a></span>
            <br />
              Page:<span><a href={'https://book-book-bice.vercel.app/'}> click here!</a></span>
            <br />
              Figma sketch:<span><a href={'https://www.figma.com/file/0fj1cBsqDnQ1gdYlhj7Vcu/Ant-Design-Components-(Copy)?node-id=10819%3A352'}> click here!</a></span>

          </ProjectText>
          <ProjectImageContainer>
            <ProjectImages src={'book1.png'} />
            <ProjectImages src={'book2.png'} />

          </ProjectImageContainer>

        </ProjectContent>

      </ProjectPageContainer>

    </ProjectPagePositioning >
  )
}

export default ProjectPage