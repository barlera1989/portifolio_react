import React from "react"
import cardsProperties from './constants'
import { useSelector, useDispatch } from 'react-redux'

import {
  setHtmlFalse,
  setCssFalse,
  setJsFalse,
  setGitFalse,
  setReactFalse,
  setPythonFalse,
  setFlaskFalse,
  setDjangoFalse,
  setCardsOnFalse,
  setCardsOffFalse
} from '../../../redux/actions'

import {
  AboutCardsInnerText,
  AboutCloseButton,
} from "./styles/cards"


const CardsDescription = () => {
  const dispatch = useDispatch()
  const html = useSelector((state) => state.html.htmlState)
  const css = useSelector((state) => state.css.cssState)
  const js = useSelector((state) => state.js.jsState)
  const git = useSelector((state) => state.git.gitState)
  const react = useSelector((state) => state.react.reactState)
  const python = useSelector((state) => state.python.pythonState)
  const flask = useSelector((state) => state.flask.flaskState)
  const django = useSelector((state) => state.django.djangoState)

  const TurnCardsOn = () => {
    dispatch(setCardsOnFalse())
    dispatch(setHtmlFalse())
    dispatch(setCssFalse())
    dispatch(setJsFalse())
    dispatch(setGitFalse())
    dispatch(setReactFalse())
    dispatch(setPythonFalse())
    dispatch(setFlaskFalse())
    dispatch(setDjangoFalse())
    dispatch(setCardsOffFalse())
  }

  const htmlText = cardsProperties[0].description
  const cssText = cardsProperties[1].description
  const javascriptText = cardsProperties[2].description
  const gitText = cardsProperties[3].description
  const reactText = cardsProperties[4].description
  const pythonText = cardsProperties[5].description
  const flaskText = cardsProperties[6].description
  const djangoText = cardsProperties[7].description


  return (
    <div>
      {html && <AboutCloseButton z_index={15} onClick={() => TurnCardsOn()}>X</AboutCloseButton>}
      {html && <AboutCardsInnerText>{htmlText.map((htmlText, i) => (
        <div key={i}>
          {htmlText}<br />
        </div>
      ))}
      </AboutCardsInnerText>}

      {css && <AboutCloseButton z_index={15} onClick={() => TurnCardsOn()}>X</AboutCloseButton>}
      {css && <AboutCardsInnerText >{cssText.map((cssText, i) => (
        <div key={i}>
          {cssText}<br />
        </div>
      ))}
      </AboutCardsInnerText>}

      {js && <AboutCloseButton z_index={15} onClick={() => TurnCardsOn()}>X</AboutCloseButton>}
      {js && <AboutCardsInnerText >{javascriptText.map((javascriptText, i) => (
        <div key={i}>
          {javascriptText}<br />
        </div>
      ))}
      </AboutCardsInnerText>}

      {git && <AboutCloseButton z_index={15} onClick={() => TurnCardsOn()}>X</AboutCloseButton>}
      {git && <AboutCardsInnerText >{gitText.map((gitText, i) => (
        <div key={i}>
          {gitText}<br />
        </div>
      ))}
      </AboutCardsInnerText>}

      {react && <AboutCloseButton z_index={15} onClick={() => TurnCardsOn()}>X</AboutCloseButton>}
      {react && <AboutCardsInnerText >{reactText.map((reactText, i) => (
        <div key={i}>
          {reactText}<br />
        </div>
      ))}
      </AboutCardsInnerText>}

      {python && <AboutCloseButton z_index={15} onClick={() => TurnCardsOn()}>X</AboutCloseButton>}
      {python && <AboutCardsInnerText >{pythonText.map((pythonText, i) => (
        <div key={i}>
          {pythonText}<br />
        </div>
      ))}
      </AboutCardsInnerText>}

      {flask && <AboutCloseButton z_index={15} onClick={() => TurnCardsOn()}>X</AboutCloseButton>}
      {flask && <AboutCardsInnerText >{flaskText.map((flaskText, i) => (
        <div key={i}>
          {flaskText}<br />
        </div>
      ))}
      </AboutCardsInnerText>}


      {django && <AboutCloseButton z_index={15} onClick={() => TurnCardsOn()}>X</AboutCloseButton>}
      {django && <AboutCardsInnerText >{djangoText.map((djangoText, i) => (
        <div key={i}>
          {djangoText}<br />
        </div>
      ))}
      </AboutCardsInnerText>}

    </div>
  )
}

export default CardsDescription