import styled from "styled-components"

export const AboutDescriptionTitleText = styled.div`
margin: 5px 40px 5px 5px;
font-family:bangers;
font-size:25px;
color: #222;
`

export const AboutDescriptionText = styled.div`

margin: 5px;
padding-top:10px;
font-family:bangers;
font-size:18px;
color: #222;
text-align: justify;
text-indent: 40px;
border-bottom: 0.5px solid gray;
`

export const AboutWorkSection = styled.div`

height:100px;
margin: 5px;
font-family:bangers;
font-size:20px;
color: #222;
`
export const AboutEducationSection = styled.div`

margin: 5px;
font-family:bangers;
font-size:18px;
color: #222;
padding-top:10px;
`