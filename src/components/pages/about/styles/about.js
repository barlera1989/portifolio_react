import styled from "styled-components"
import { motion } from "framer-motion"


export const AboutPagePositioning = styled.div`
display:flex;
align-items: center;
justify-content: center;
width:70vw;
@media(max-width:600px)
  {
    width:100vw;
  }
`

export const AboutPageContainer = styled.div`
height: 90vh;
display: flex;
flex-direction: row;
align-items:center;
@media(max-width:600px)
  {
    flex-direction: column;
    height:auto;
  }
`


export const AboutRightContent = styled.div`
height:500px;
width:300px;
border: 2px solid lightgray;
border-radius:10px;
margin:10px;
display: flex;
flex-direction: column;
@media(max-width:600px)
  {
    margin:10px;
  }
`


export const AboutLeftContent = styled(motion.div)`
height:500px;
width:300px;
border: 2px solid lightgray;
border-radius:10px;
margin:20px;
background-color: rgb(230,230,230);
@media(max-width:600px)
  {
    margin:40px 0px;
  }
`