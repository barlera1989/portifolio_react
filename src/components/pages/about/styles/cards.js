import styled from "styled-components"
import { motion } from "framer-motion"


export const AboutCardsMainContainer = styled(motion.div)`
width:100%;
height:100%;
display:flex;
align-items: center;
justify-content: center;
z-index: ${(props) => `${props.z_index}`};
background-color: white;
`


export const AboutCardsTipText = styled.div`
padding-top:10px;
font-family:bangers;
font-size:22px;
color: #222;
position:absolute;
text-align: center;
`

export const AboutCardsContainer = styled.div`
height: 110px;
width:85px;
position:absolute;
`

export const AboutCardsInnerContainer = styled(motion.div)`
height: 110px;
width:85px;
position:absolute;
z-index: ${(props) => `${props.z_index}`};
:hover{

  border-top: ${(props) => `${props.hovertop}px solid white`};
  border-left: ${(props) => `${props.hoverleft}px solid white`};
  cursor: ${(props) => `${props.cursor}`};

}
`

export const AboutCardsImage = styled.img`
height: 85px;
width:85px;
background-color: white;
`

export const AboutCardsText = styled.div`
height: 20px;
width:85px;
font-size: 20px;
text-align: center;
font-family:bangers;
opacity: 1.0;
background-color: white;
:hover{
  cursor: default;
}
`

export const AboutCardsInnerText = styled(motion.div)`
font-size: 20px;
font-family:bangers;
position:absolute;

`
export const AboutCloseButton = styled(motion.button)`
height:25px;
position: absolute;
text-align:center;
margin-top:10px;
margin-left: 260px;
z-index: ${(props) => `${props.z_index}`};
:hover{
  cursor: pointer;
}
`
export const AboutLink = styled.a`
font-size: 20px;
font-family:bangers;
position:absolute;
z-index: ${(props) => `${props.z_index}`};
:hover{
  cursor: pointer;
}
`