const cardsProperties = [
  {

    item: 'HTML 5',
    x: -100,
    y: -120,
    z: 13,
    delay: 1,
    src: 'html.png',
    state: 'Html',
    description: ['HTML SKILLS', '', '- Use of lots of html tags', '- Child/parent organization for', 'flexbox usage', '- Use of Ids and classes']
  },

  {

    item: 'CSS 3',
    x: 0,
    y: -120,
    z: 12,
    delay: 1.1,
    src: 'css.png',
    state: 'Css',
    description: ['CSS SKILLS', '', '- Flexbox', '- MediaQueries', '- Responsive content', '- Absolute and Relative positioning like', 'vw , vh, %, etc...']
  },

  {

    item: 'JAVASCRIPT',
    x: 100,
    y: -120,
    z: 11,
    delay: 1.2,
    src: 'javascript.png',
    state: 'Javascript',
    description: ['JAVASCRIPT SKILLS', '', '- Good skills with functions', '- Loops and conditionals', '- Skills with DOM', '- Skills with events']
  },

  {

    item: 'GIT',
    x: -65,
    y: 0,
    z: 10,
    delay: 1.3,
    src: 'git.png',
    state: 'Git',
    description: ['GIT SKILLS', '', '- Work on different branchs for', 'better oragnization', '- Skills with merging branches', '- GitFlow']
  },

  {

    item: 'REACT',
    x: 65,
    y: 0,
    z: 9,
    delay: 1.4,
    src: 'react.png',
    state: 'React',
    description: ['REACT SKILLS', '', '- REACT-ROUTER-DOM', '- STYLED-COMPONENTS', '- REDUX', '- USE STATE / USE EFFECT', '- MOTION', '- ANTD FORMS']
  },

  {

    item: 'PYTHON',
    x: -100,
    y: 120,
    z: 8,
    delay: 1.5,
    src: 'python.png',
    state: 'Python',
    description: ['PYTHON SKILLS', '', '- Good skills with functions,', '- Loops for and conditionals', '- Skills with list/tuples/dictionaries', '- Skills working on Csv', '- Skills with POO']
  },

  {

    item: 'FLASK',
    x: 0,
    y: 120,
    z: 7,
    delay: 1.6,
    src: 'flask.png',
    state: 'Flask',
    description: ['FLASK SKILLS', '', '- Skills requesting API with', 'html forms or json in insomnia', '- Jinja', '- Flask Alchemy and its components', '- ORM']
  },

  {

    item: 'DJANGO',
    x: 100,
    y: 120,
    z: 7,
    delay: 1.7,
    src: 'django.png',
    state: 'Django',
    description: ['DJANGO SKILLS', '', 'Under Construction']
  },
]

export default cardsProperties