import React from "react"
import cardsProperties from './constants'
import CardsDescription from './cardsDescription'
import { useSelector, useDispatch } from 'react-redux'

import {
  setHtmlTrue,
  setCssTrue,
  setJsTrue,
  setGitTrue,
  setReactTrue,
  setPythonTrue,
  setFlaskTrue,
  setDjangoTrue,
  setCardsOnTrue,
  setCardsOffTrue,
} from '../../../redux/actions'

import {
  AboutPagePositioning,
  AboutPageContainer,
  AboutLeftContent,
  AboutRightContent,

} from "./styles/about"

import {
  AboutDescriptionText,
  AboutDescriptionTitleText,
  AboutWorkSection,
  AboutEducationSection,
} from "./styles/text"

import {
  AboutCardsMainContainer,
  AboutCardsContainer,
  AboutCardsInnerContainer,
  AboutCardsImage,
  AboutCardsTipText,
  AboutCardsText

} from "./styles/cards"

/* 
TO DO:

3 - make another styles, put in a folder

 */


const AboutPage = () => {
  const dispatch = useDispatch()

  const cardsOn = useSelector((state) => state.cardsOn.cardsOnState)
  const cardsOff = useSelector((state) => state.cardsOff.cardsOffState)

  const changeCardView = (stateFunc) => {


    dispatch(setCardsOnTrue())

    if (cardsOff === false) {

      setTimeout(TurnCardsOff, 500, stateFunc)

    }

    dispatch(setCardsOffTrue())
  }

  const TurnCardsOff = (stateFunc) => {
    if (stateFunc === "Html") {
      dispatch(setHtmlTrue())
    }

    if (stateFunc === "Css") {
      dispatch(setCssTrue())
    }

    if (stateFunc === "Javascript") {
      dispatch(setJsTrue())
    }

    if (stateFunc === "Git") {
      dispatch(setGitTrue())
    }

    if (stateFunc === "React") {
      dispatch(setReactTrue())
    }

    if (stateFunc === "Python") {
      dispatch(setPythonTrue())
    }

    if (stateFunc === "Flask") {
      dispatch(setFlaskTrue())
    }

    if (stateFunc === "Django") {
      dispatch(setDjangoTrue())
    }
  }

  return (
    <AboutPagePositioning>
      <AboutPageContainer >

        <AboutLeftContent >
          {!cardsOn && <AboutCardsTipText>CLick in the icons to see my skills!</AboutCardsTipText>}
          <CardsDescription />

          <AboutCardsMainContainer z_index={-1} animate={{ opacity: cardsOn ? 0.05 : 1 }} transition={{ duration: 0.5 }}>
            <AboutCardsContainer >
              {cardsProperties.map((cardsProperties, i) => (
                <AboutCardsContainer key={i}>
                  <AboutCardsInnerContainer
                    hovertop={cardsOn ? 0 : 3}
                    hoverleft={cardsOn ? 0 : 3}
                    cursor={cardsOn ? 'arrow' : 'pointer'}
                    onClick={() => changeCardView(cardsProperties.state)}
                    z_index={cardsProperties.z}
                    animate={{ y: cardsProperties.y, x: cardsProperties.x, scale: 1 }}
                    transition={{ duration: 0.25, delay: cardsProperties.delay }}>
                    <AboutCardsImage src={cardsProperties.src}></AboutCardsImage>
                    <AboutCardsText>{cardsProperties.item}</AboutCardsText>
                  </AboutCardsInnerContainer>
                </AboutCardsContainer>

              ))}



            </AboutCardsContainer>
          </AboutCardsMainContainer>
        </AboutLeftContent>

        <AboutRightContent>
          <AboutDescriptionTitleText >WELCOME TO MY SITE!!!</AboutDescriptionTitleText>

          <AboutDescriptionText>MY NAME IS RAFAEL AND My professional target is grow up in Technologies area,
          in hard-skills as well as soft-skills, to create a real efficient and enjoyable work environment .
            That why i'm look for oportunities that offer me growing up in this direction.</AboutDescriptionText>

          <AboutDescriptionTitleText>ACADEMIC FORMATION</AboutDescriptionTitleText>

          <AboutEducationSection>KENZIE ACADEMY BRASIL

            <br></br>DESCRIPTION:

            <div>Full-Stack Developer course that works in front-end and back-end technologies.
HTML5, CSS3, JavaScript (ES6+), React, Python (Django e Flask) and SQL</div>

          </AboutEducationSection>

          <AboutWorkSection></AboutWorkSection>


        </AboutRightContent>




      </AboutPageContainer>

    </AboutPagePositioning >
  )
}

export default AboutPage