import styled from "styled-components"
import { motion } from "framer-motion"

export const HomePagePositioning = styled.div`
display:flex;
align-items: center;
justify-content: center;
width:70vw;
@media(max-width:600px)
  {
    width:95vw;
  }
`



export const HomePageContainer = styled(motion.div)`
height: 100vh;
display: flex;
flex-direction: column;
align-items:center;
justify-content: center;
`

export const HomeUpperText = styled.div`
height:40px;
margin: 0px;
font-family:bangers;
font-size:30px;
color: #444;
`


export const HomeCenterPicture = styled.img`
border: 1px solid black;
height:200px;
width: 250px;
margin: 0px;
`


export const HomeLowerContainer = styled.div`
height: 120px;
position: flex;
flex-direction: column;
margin: 0px;
`

export const HomeLowerText = styled.div`
height:30px;
width: 250px;
margin: 5px;
font-family:bangers;
font-size:25px;
text-align: center;
color: #444;
`

export const HomeLowerLink = styled.div`
height:30px;
width: 250px;
margin: 5px;
font-family:bangers;
font-size:25px;
text-align: center;
color: #333;
:hover{
  cursor: pointer;
  background-color: #CFD;
  transition: 1.0s;
  color: #111;
}
`



export const HomeLinksContainer = styled.div`
display: flex;
flex-direction:row;
`



export const HomeLinksPictures = styled.a`
margin:15px;
color: rgb(50,50,200);

`