import React from "react"
import { useHistory } from 'react-router-dom'

import { FaFacebook } from "react-icons/fa"
import { AiFillGithub, AiFillGitlab, AiFillLinkedin } from "react-icons/ai"


import {
  HomePageContainer,
  HomeUpperText,
  HomeCenterPicture,
  HomeLowerContainer,
  HomeLowerText,
  HomeLinksContainer,
  HomeLinksPictures,
  HomePagePositioning,
  HomeLowerLink

} from "./styles"

const HomePage = () => {
  const history = useHistory()


  return (
    <HomePagePositioning>
      <HomePageContainer animate={{ scale: [0.1, 1], rotate: [0, 360, 720, 0, 0, 0, 0, 0, 0, 0] }} transition={{ duration: 1 }}>

        <HomeUpperText> Rafael Barlera Alves</HomeUpperText>
        <HomeUpperText> FULL STACK DEVELOPER</HomeUpperText>

        <HomeCenterPicture src='typing-cat.gif'></HomeCenterPicture>

        <HomeLowerContainer>
          <HomeLowerText>CSS | REACT | REDUX  </HomeLowerText>
          <HomeLowerText>PYTHON | FLASK | DJANGO</HomeLowerText>
          <HomeLowerLink onClick={() => history.push('/about')}>CLICK HERE TO SEE MORE!</HomeLowerLink>
        </HomeLowerContainer>

        <HomeLinksContainer>
          <HomeLinksPictures href='https://www.linkedin.com/in/rafael-barlera/'>
            <AiFillLinkedin size={40}></AiFillLinkedin>
          </HomeLinksPictures>
          <HomeLinksPictures href='https://www.facebook.com/rafael.barleraalves/'>
            <FaFacebook size={40}></FaFacebook>
          </HomeLinksPictures>
          <HomeLinksPictures href='https://github.com/Barlera1989'>
            <AiFillGithub size={40}></AiFillGithub>
          </HomeLinksPictures>
          <HomeLinksPictures href='https://gitlab.com/barlera1989'>
            <AiFillGitlab size={40}></AiFillGitlab>
          </HomeLinksPictures>

        </HomeLinksContainer>

      </HomePageContainer>

    </HomePagePositioning>
  )
}

export default HomePage