import React from "react"
import Header from './header'
import styled from 'styled-components'
import HomePage from './pages/home'
import AboutPage from './pages/about'
import ContactPage from './pages/contact'
import ProjectPage from './pages/projects'
import { Switch, Route } from 'react-router-dom'

const Pages = () => {

  return (
    <Switch>
      <Route exact path='/'>

        <MainPage>
          <Header />
          <HomePage />
        </MainPage>

      </Route>

      <Route exact path='/about'>

        <MainPage>
          <Header />
          <AboutPage />

        </MainPage>

      </Route>
      <Route exact path='/projects'>

        <MainPage>
          <Header />
          <ProjectPage />

        </MainPage>

      </Route>
      <Route exact path='/contact'>

        <MainPage>
          <Header />
          <ContactPage />
        </MainPage>

      </Route>
    </Switch>
  )
}

export default Pages



const MainPage = styled.div`
display: flex;
flex-direction:row;
@media(max-width:600px)
  {
    flex-direction:column;
    width:96vw;
  }
`