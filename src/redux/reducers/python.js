import { PYTHONFALSE, PYTHONTRUE } from "../actions"

const defaultState = {
  pythonState: false
}

const python = (state = defaultState, action) => {
  switch (action.type) {
    case PYTHONFALSE:
      return {
        ...state,
        pythonState: false
      }
    case PYTHONTRUE:
      return {
        ...state,
        pythonState: true
      }
    default:
      return state
  }
}

export default python