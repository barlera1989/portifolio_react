import { CARDSONTRUE, CARDSONFALSE } from "../actions"

const defaultState = {
  cardsOnState: false
}

const cardsOn = (state = defaultState, action) => {
  switch (action.type) {
    case CARDSONFALSE:
      return {
        ...state,
        cardsOnState: false
      }
    case CARDSONTRUE:
      return {
        ...state,
        cardsOnState: true
      }
    default:
      return state
  }
}

export default cardsOn