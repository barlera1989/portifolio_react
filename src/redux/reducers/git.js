import { GITFALSE, GITTRUE } from "../actions"

const defaultState = {
  gitState: false
}

const git = (state = defaultState, action) => {
  switch (action.type) {
    case GITFALSE:
      return {
        ...state,
        gitState: false
      }
    case GITTRUE:
      return {
        ...state,
        gitState: true
      }
    default:
      return state
  }
}

export default git