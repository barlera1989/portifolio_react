import { FLASKFALSE, FLASKTRUE } from "../actions"

const defaultState = {
  flaskState: false
}

const flask = (state = defaultState, action) => {
  switch (action.type) {
    case FLASKFALSE:
      return {
        ...state,
        flaskState: false
      }
    case FLASKTRUE:
      return {
        ...state,
        flaskState: true
      }
    default:
      return state
  }
}

export default flask