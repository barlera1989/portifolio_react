import { REACTFALSE, REACTTRUE } from "../actions"

const defaultState = {
  reactState: false
}

const react = (state = defaultState, action) => {
  switch (action.type) {
    case REACTFALSE:
      return {
        ...state,
        reactState: false
      }
    case REACTTRUE:
      return {
        ...state,
        reactState: true
      }
    default:
      return state
  }
}

export default react