import { CSSTRUE, CSSFALSE } from "../actions"

const defaultState = {
  cssState: false
}

const css = (state = defaultState, action) => {
  switch (action.type) {
    case CSSFALSE:
      return {
        ...state,
        cssState: false
      }
    case CSSTRUE:
      return {
        ...state,
        cssState: true
      }
    default:
      return state
  }
}

export default css