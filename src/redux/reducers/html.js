import { HTMLFALSE, HTMLTRUE } from "../actions"

const defaultState = {
  htmlState: false
}

const html = (state = defaultState, action) => {
  switch (action.type) {
    case HTMLFALSE:
      return {
        ...state,
        htmlState: false
      }
    case HTMLTRUE:
      return {
        ...state,
        htmlState: true
      }
    default:
      return state
  }
}

export default html