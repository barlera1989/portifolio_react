import { DJANGOFALSE, DJANGOTRUE } from "../actions"

const defaultState = {
  djangoState: false
}

const django = (state = defaultState, action) => {
  switch (action.type) {
    case DJANGOFALSE:
      return {
        ...state,
        djangoState: false
      }
    case DJANGOTRUE:
      return {
        ...state,
        djangoState: true
      }
    default:
      return state
  }
}

export default django