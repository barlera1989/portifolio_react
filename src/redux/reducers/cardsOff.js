import { CARDSOFFTRUE, CARDSOFFFALSE } from "../actions"

const defaultState = {
  cardsOffState: false
}

const cardsOff = (state = defaultState, action) => {
  switch (action.type) {
    case CARDSOFFFALSE:
      return {
        ...state,
        cardsOffState: false
      }
    case CARDSOFFTRUE:
      return {
        ...state,
        cardsOffState: true
      }
    default:
      return state
  }
}

export default cardsOff