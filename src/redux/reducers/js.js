import { JSFALSE, JSTRUE } from "../actions"

const defaultState = {
  jsState: false
}

const js = (state = defaultState, action) => {
  switch (action.type) {
    case JSFALSE:
      return {
        ...state,
        jsState: false
      }
    case JSTRUE:
      return {
        ...state,
        jsState: true
      }
    default:
      return state
  }
}

export default js