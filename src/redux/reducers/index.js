import { combineReducers } from "redux";
import html from './html'
import css from './css'
import js from './js'
import git from './git'
import react from './react'
import python from './python'
import flask from './flask'
import django from './django'
import cardsOn from './cardsOn'
import cardsOff from './cardsOff'

export default combineReducers({ html, css, js, git, react, python, flask, django, cardsOn, cardsOff });
