export const HTMLTRUE = 'HTMLTRUE'
export const HTMLFALSE = 'HTMLFALSE'

export const CSSTRUE = 'CSSTRUE'
export const CSSFALSE = 'CSSFALSE'

export const JSTRUE = 'JSTRUE'
export const JSFALSE = 'JSFALSE'

export const GITTRUE = 'GITTRUE'
export const GITFALSE = 'GITFALSE'

export const REACTTRUE = 'REACTTRUE'
export const REACTFALSE = 'REACTFALSE'

export const PYTHONTRUE = 'PYTHONTRUE'
export const PYTHONFALSE = 'PYTHONFALSE'

export const FLASKTRUE = 'FLASKTRUE'
export const FLASKFALSE = 'FLASKFALSE'

export const DJANGOTRUE = 'DJANGOTRUE'
export const DJANGOFALSE = 'DJANGOFALSE'

export const CARDSONTRUE = 'CARDSONTRUE'
export const CARDSONFALSE = 'CARDSONFALSE'

export const CARDSOFFTRUE = 'CARDSOFFTRUE'
export const CARDSOFFFALSE = 'CARDSOFFFALSE'




export const setHtmlTrue = () => ({
  type: HTMLTRUE
});

export const setHtmlFalse = () => ({
  type: HTMLFALSE
});





export const setCssTrue = () => ({
  type: CSSTRUE
});


export const setCssFalse = () => ({
  type: CSSFALSE
});





export const setJsTrue = () => ({
  type: JSTRUE
});

export const setJsFalse = () => ({
  type: JSFALSE
});





export const setGitTrue = () => ({
  type: GITTRUE
});

export const setGitFalse = () => ({
  type: GITFALSE
});





export const setReactTrue = () => ({
  type: REACTTRUE
});

export const setReactFalse = () => ({
  type: REACTFALSE
});





export const setPythonTrue = () => ({
  type: PYTHONTRUE
});

export const setPythonFalse = () => ({
  type: PYTHONFALSE
});




export const setFlaskTrue = () => ({
  type: FLASKTRUE
});

export const setFlaskFalse = () => ({
  type: FLASKFALSE
});

export const setDjangoTrue = () => ({
  type: DJANGOTRUE
});

export const setDjangoFalse = () => ({
  type: DJANGOFALSE
});




export const setCardsOnTrue = () => ({
  type: CARDSONTRUE
});

export const setCardsOnFalse = () => ({
  type: CARDSONFALSE
});



export const setCardsOffTrue = () => ({
  type: CARDSOFFTRUE
});

export const setCardsOffFalse = () => ({
  type: CARDSOFFFALSE
});