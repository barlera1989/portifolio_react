1 - HEADERS

1.1 PICTURE OK
1.2 MENUS OK
1.3 MENU ICONS OK
1.4 WORKING LINKS
1.5 LAYOUT

-------------------------------

2 - HOME

2.1 - NAME ok
2.2 - GIF ??
2.3 - COMPETENCY LIST ??
2.3.1 - LINK TO ABOUT
2.4 - SOCIAL LINKS


--------------------------------

3 - ABOUT

2.1 - ABOUT TEXT
2.2 - TECNOLOGIES
2.2.1 - TEC CARDS

a. html
b. css
c. javascript
d. git

e. react

e1. styled-components
e2. framer-motion
e3. redux




f. python
g. flask
h. pyautocad


2.2.2 - ANIMATED TEC POSITIONS
2.2.3 - TEC DESCRIPTIONS ON HOVER
2.3 - EDUCATION
2.4 - WORKS

----------------------------------

4 - PORTIFOLIO

4.1 - LIG-4
4.2 - BOOKBOOK
4.3 - GITLAB LINKS
4.4 - UNDER CONSTRUCTION

-----------------------------------

5 - CONTACT

5.1 ADRESS (GOOGLE MAPS API)
5.2 - CONTACTS LIST : WHATSAPP, FACEBOOK, LINKEDIN, GITHUB
5.3 MAIL TO ME
5.3.1 - OPEN FORMS ON CLICK
5.3.2 - SEND FORMS



------------------------------------

1 - PROJETOS RELEVANTES:

LIG-4 : Primeiro projeto em grupo usando HTML / CSS / JS 

BOOK-BOOK: Projeto em grupo com react

ESTE PORTIFOLIO: React

python/flask